﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automaion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KeyboardListener KListener = new KeyboardListener();
        List<string> lst = new List<string>();
        public MainWindow()
        {
            KListener.KeyDown += new RawKeyEventHandler(KListener_KeyDown);
            KListener.KeyUp += new RawKeyEventHandler(KListener_KeyUP);
            InitializeComponent();
        }
        public void KListener_KeyDown(object sender, RawKeyEventArgs args)
        {


        }
        public void KListener_KeyUP(object sender, RawKeyEventArgs args)
        {
            string keytext = string.Empty;
            if (args.Key == System.Windows.Input.Key.Space)
            {
                keytext = " ";
            }
            else if (args.Key == System.Windows.Input.Key.Enter)
            {
                keytext = "\n";
            }
            else
            {
                keytext = args.Key.ToString();
            }
            if (args.Key == System.Windows.Input.Key.Back)
            {
                if (lst.Count > 0)
                    lst.RemoveAt(lst.Count - 1);
            }
            else
            {
                lst.Add(keytext);
            }
            txtlog.Text = string.Join("", lst);

        }

    }
}
